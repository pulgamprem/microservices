package com.microservices.features.mapper;

import com.microservices.features.entity.User;
import com.microservices.features.model.UserDTO;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserMapper {
    public UserDTO userDAOToDTO(User user){
        return UserDTO.builder()
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .build();
    }
    public List<UserDTO> userDAOToDTO(Iterable<User> userDAOList){
        List<UserDTO> users = new ArrayList<>();
        userDAOList.forEach(user -> users.add(userDAOToDTO(user)));
        return users;
    }
}
