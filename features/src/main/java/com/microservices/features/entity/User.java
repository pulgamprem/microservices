package com.microservices.features.entity;

import jakarta.persistence.*;
import lombok.Data;

@Table(name = "t_user")
@Data
@Entity
public class User {
    @Id
    @Column(name = "user_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @Column(name = "first_name")
    String firstName;
    @Column(name = "last_name")
    String lastName;
}
