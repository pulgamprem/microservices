package com.microservices.features.service;

import com.microservices.features.mapper.UserMapper;
import com.microservices.features.model.UserDTO;
import com.microservices.features.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.util.List;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    UserMapper userMapper;

    public List<UserDTO> getUsers() {
        return userMapper.userDAOToDTO(userRepository.findAll());
    }

    public Flux<UserDTO> getUsersReactive() {
        /*return Flux.fromIterable(userRepository.findAll())
                .map(userMapper::userDAOToDTO);*/
        return Flux.range(1, 10000)
                .map(value -> UserDTO.builder().firstName("Prem").lastName(String.valueOf(value)).build())
                .doOnNext(System.out::println);
    }
}
