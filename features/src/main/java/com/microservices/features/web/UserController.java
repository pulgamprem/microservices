package com.microservices.features.web;

import com.microservices.features.model.UserDTO;
import com.microservices.features.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.List;
import java.util.Optional;

@RestController
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/sample-user")
    ResponseEntity<String> getUserName(){
        return ResponseEntity.of(Optional.of("Hero"));
    }

    @GetMapping("/users")
    ResponseEntity<List<UserDTO>> getUsers(){
        return ResponseEntity.of(Optional.ofNullable(userService.getUsers()));
    }

    @GetMapping(value = "/users-reactive", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    Flux<UserDTO> getUsersReactive(){
        return userService.getUsersReactive()
                .delayElements(Duration.ofMillis(100));
    }
}
