package com.microservices.features;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Disabled
class FeaturesApplicationTests {

	@Test
	void contextLoads() {
	}

}
