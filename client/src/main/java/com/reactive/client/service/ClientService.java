package com.reactive.client.service;

import com.reactive.client.adaptor.UserAdaptor;
import com.reactive.client.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.OffsetDateTime;

@Service
@Slf4j
public class ClientService {

    @Autowired
    UserAdaptor userAdaptor;

    @Scheduled(cron = "0/30 * * ? * *")
    public void printUsers() {
        log.debug("Schedule triggered {}", OffsetDateTime.now());
        //userAdaptor.getUsers().getBody().forEach(user -> log.info(user.toString()));

        /*userAdaptor.getUsersReactive()
                .doOnNext(user -> log.info(user.toString()))
                .subscribe();*/

        WebClient client = WebClient.builder()
                .baseUrl("http://localhost:8080")
                .build();

        client.get().uri("/users-reactive")
                .retrieve()
                .bodyToFlux(User.class)
                .doOnNext(user ->log.debug("user: {}"+ user.toString()))
                .subscribe();     // Logs everything at once without any delay
//                .blockLast();     //waits for 15 sec (3*5sec) before it logs everything
//                .blockFirst();   // fetched only first element

//        users.collectList().doOnNext(l1 -> System.out.println(">> Get Users 1=" + l1)).block();
    }
}
