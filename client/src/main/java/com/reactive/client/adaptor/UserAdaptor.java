package com.reactive.client.adaptor;

import com.reactive.client.model.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import reactor.core.publisher.Flux;

import java.util.List;

@FeignClient(value = "users", url = "http://localhost:8080/")
public interface UserAdaptor {

    @RequestMapping(method = RequestMethod.GET, value = "users")
    ResponseEntity<List<User>> getUsers();

    @RequestMapping(method = RequestMethod.GET, value = "users-reactive")
    Flux<User> getUsersReactive();
}